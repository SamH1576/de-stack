Title: Home
Date: 2010-12-03 10:20
Author: Sam Hiscox
save_as: index.html

**Data Engineering is:**

-   CompSci fundamentals
-   SQL
-   Software Engineering (SWE)

**You’ll also be tested on:**

-   Problem solving skills
-   Personal experiences (_tell me about a time when_)

---

You'll be tested on each of these components in the following ways:

---

#### CompSci fundamentals

-   Whiteboard programming
-   Pairprogramming
-   LeetCode/HackerRank questions

---

#### SQL

-   Case study (give you some data, have you pre it)
-   Whiteboarding/pairprogramming
-   Q&A

---

#### SWE

-   Ask you to submit some code before the interview
-   Describe (best) practices for:
    -   Version control (git)
    -   Testing
    -   Formatting / linting
-   Comment on what makes good code
    -   DRY
    -   Modular
    -   Short functions
    -   Well documented (docstrings / comment)

### To get up to speed on each of these components:

1. CompSci fundamentals
    - Learn about data structures using udemy or something free
    - Practice on HackerRank / leetcode / others
    - Watch YouTube videos
    - Practice
    - Learn about computational complexity (O(n) etc.)
    - _Practice_
2. SQL
    - Online resources like datacamp etc.
    - Hackerrank
    - Know windowing, group by, different joins etc.
3. SWE
    - Contribute to OSS
    - Make small projects, ask SW friends to review and give feedback
    - Try incorporate known elements of the stack
    - Get familiar with using docker
    - Add CI to your personal projects if you don’t have an opportunity to try it at work
    - Watch videos by people like Uncle Bob on clean code
        - Get some familiarity coding first
    - Do clean code courses like [this](https://www.freecodecamp.org/news/clean-coding-for-beginners/) one

---

### Language / Tech

A suggested list of things to learn:

1. Python
2. SQL
3. Docker
4. Pyspark
    - _Note [kedro-docker](https://github.com/kedro-org/kedro-plugins/tree/main/kedro-docker) is an easy way to get started running pyspark locally_
5. Some SQL database like Postgres or RDS
6. Some NoSQL database like MongoDB or DynamoDB
7. A sprinkling of HTML/CSS/JS is always handy
    - [This](https://www.udemy.com/course/the-web-developer-bootcamp/) course comes highly recommended and includes some MongoDB
